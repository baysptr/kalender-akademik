<?php

use Illuminate\Database\Seeder;
use App\Warna;

class WarnaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Warna::insert([
           [
               'warna_background'=>"#ff8080",
               'warna_font'=>"#ffffff",
               'warna_nama'=>"merah",
           ],
            [
                'warna_background'=>"#8533ff",
                'warna_font'=>"#ffffff",
                'warna_nama'=>"hijau",
            ]
        ]);
    }
}
