<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::insert([
           [
               "name"=>"Superadmin",
               "email"=>"admin@uht.ac.id",
               "username"=>"superadmin",
               "password"=>Hash::make("password")
           ]
        ]);
    }
}
