<?php

use Illuminate\Database\Seeder;
use App\Event;
use Carbon\Carbon;
use Faker\Factory as Faker;

class EventTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tanggal = Carbon::now();
        $faker = Faker::create('id_ID');
        Event::insert([
            [
                "warna_id"=>1,
                "tanggal"=> $tanggal->addDays(random_int(3,10))->toDateString(),
                "event"=> $faker->name
            ],
            [
                "warna_id"=>2,
                "tanggal"=> $tanggal->addDays(random_int(3,10))->toDateString(),
                "event"=> $faker->name
            ],
            [
                "warna_id"=>1,
                "tanggal"=> $tanggal->addDays(random_int(3,10))->toDateString(),
                "event"=> $faker->name
            ],
            [
                "warna_id"=>2,
                "tanggal"=> $tanggal->addDays(random_int(3,10))->toDateString(),
                "event"=> $faker->name
            ]
        ]);
    }
}
