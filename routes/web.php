<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@home')->name('page.welcome');

Route::get('/login', function () {
    return view('login');
});

Route::post('login', 'AuthController@login')->name('login');
Route::get('logout', 'AuthController@logout')->name('logout');

Route::middleware('auth')->group(function () {
    Route::get('/home', 'EventController@index')->name('home');
    Route::get('/event/show/{id}', 'EventController@show')->name('show.event');
    Route::post('/event/store', 'EventController@store')->name('store.event');
    Route::post('/event/destroy', 'EventController@destroy')->name('destroy.event');

    Route::get('/color', 'ColorController@index')->name('warna');
    Route::get('/color/show/{id}', 'ColorController@show')->name('show.warna');
    Route::post('/color/store', 'ColorController@store')->name('store.warna');
    Route::post('/color/destroy', 'ColorController@destroy')->name('destroy.warna');
});
