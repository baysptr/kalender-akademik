<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    public function warna(){
        return $this->belongsTo("App\Warna");
    }
}
