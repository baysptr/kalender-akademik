<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(Request $request){
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            // Authentication passed...
            return redirect()->intended('home');
        }else{
            return back()->with(['error'=>'email dan password anda tidak dikenali']);
        }
    }

    public function logout(){
        Auth::logout();
        return redirect(route('login'));
    }
}
