<?php

namespace App\Http\Controllers;

use App\Event;
use App\Warna;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home(){
        $event = Event::with('warna')->latest()->get();
        $warna = Warna::latest()->get();
        return view('home', ['events'=>$event, 'warnas'=>$warna]);
    }
}
