<?php

namespace App\Http\Controllers;

use App\Warna;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class ColorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Warna::with('event')->latest()->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('warna_background', function ($q) {
                    return "<span style='background-color: " . $q->warna_background . "; padding: 8px'>" . $q->warna_background . "</span>";
                })
                ->addColumn('warna_font', function ($q) {
                    return "<span style='background-color: " . $q->warna_font . "; padding: 8px'>" . $q->warna_font . "</span>";
                })
                ->addColumn('btn_edit', function ($q){
                    $btn = '<button onclick="editHub('.$q->id.')" class="btn btn-block btn-warning">Edit</button>';
                    return $btn;
                })
                ->addColumn('btn_hapus', function ($q){
                    if(count($q->event)>0){
                        $btn = '<span>Data ini tidak dapat dihapus</span>';
                    }else{
                        $btn = '<button onclick="hapusHub('.$q->id.')" class="btn btn-block btn-danger">Hapus</button>';
                    }
                    return $btn;
                })
                ->rawColumns(['warna_background', 'warna_font', 'btn_edit', 'btn_hapus'])
                ->make(true);
        }

        return view('color');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->id;
        $background = $request->background;
        $font = $request->font;
        $nama = $request->nama;
        $deskripsi = $request->deskripsi;

        try {
            DB::beginTransaction();
            if ($id == null) {
                $HUBcreate = new Warna();
            } else {
                $HUBcreate = Warna::find($id);
            }
            $HUBcreate->warna_background = $background;
            $HUBcreate->warna_font = $font;
            $HUBcreate->warna_nama = $nama;
            $HUBcreate->deskripsi = $deskripsi;
            $HUBcreate->save();

            DB::commit();
        } catch (\Exception $exception) {
            return response()->json(['msg' => 'error simpan data', 'code' => '0']);
        }

        return response()->json(['msg' => 'berhasil', 'code' => '1']);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Warna::findOrFail($id);
        return response()->json(['data' => $data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            DB::beginTransaction();
            $hub = Warna::findOrFail($request->id);
            $hub->delete();

            DB::commit();
        } catch (\Exception $exception) {
            return response()->json(['msg' => 'error simpan data', 'code' => '0']);
        }

        return response()->json(['msg' => 'berhasil', 'code' => '1']);
    }
}
