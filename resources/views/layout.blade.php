<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Panel Kalender Akademik | UHT</title>

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link href="https://getbootstrap.com/docs/4.6/examples/navbar-fixed/navbar-top-fixed.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css" rel="stylesheet">
    <script>
        var viewMode = getCookie("view-mode");
        if(viewMode == "desktop"){
            viewport.setAttribute('content', 'width=1024');
        }else if (viewMode == "mobile"){
            viewport.setAttribute('content', 'width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no');
        }
    </script>
</head>
<body>

<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="#">
        <img src="{{asset('banner.png')}}" class="d-inline-block align-top" width="15%">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item @yield('home')">
                <a class="nav-link" href="{{route('home')}}">Home</a>
            </li>
            <li class="nav-item @yield('warna')">
                <a class="nav-link" href="{{route('warna')}}">Warna</a>
            </li>
        </ul>
        <div class="form-inline mt-2 mt-md-0">
            <a href="{{route('logout')}}">
                <button class="btn btn-outline-warning my-2 my-sm-0" type="button">Logout</button>
            </a>
        </div>
    </div>
</nav>

<main role="main" class="container"><br/><br/>
    @yield('content')
{{--    <div class="jumbotron">--}}
{{--        <h1>Navbar example</h1>--}}
{{--        <p class="lead">This example is a quick exercise to illustrate how fixed to top navbar works. As you scroll, it will remain fixed to the top of your browser’s viewport.</p>--}}
{{--        <a class="btn btn-lg btn-primary" href="/docs/4.6/components/navbar/" role="button">View navbar docs &raquo;</a>--}}
{{--    </div>--}}
</main>


<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>

<script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js" type="text/javascript"></script>

@stack('javascript')

</body>
</html>
