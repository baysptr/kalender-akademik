@extends('layout')

@section('home', 'active')

@section('content')

    <div class="jumbotron">
        <h1>Kegiatan Akademik</h1>
        <button class="btn btn-lg btn-primary" onclick="openModal()">Tambah Kegiatan</button>
    </div>
    <table class="table table-bordered" id="content">
        <thead>
        <tr>
            <th>No</th>
            <th>Warna Background</th>
            <th>Warna Font</th>
            <th>Tanggal</th>
            <th>Kegiatan</th>
            <th></th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" onclick="closeModal()">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="eventForm">
                        @csrf
                        <input type="hidden" name="id" id="id">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Warna</label>
                                <select name="warna" id="warna" class="form-control">
                                    <option selected disabled>-- Pilih Warna --</option>
                                    @foreach($warnas as $warna)
                                        <option value="{{$warna->id}}">{{$warna->warna_nama}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Tanggal</label>
                                <input type="date" name="tanggal" id="tanggal" class="form-control">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label>Deskripsi</label>
                                <textarea class="form-control" name="deskripsi" id="deskripsi" rows="5"></textarea>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" onclick="closeModal()">Close</button>
                    <button type="button" class="btn btn-primary" onclick="simpanHub()" id="btnSimpan"></button>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('javascript')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#content').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('home') }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'warna_background'},
                    {data: 'warna_font'},
                    {data: 'tanggal'},
                    {data: 'event'},
                    {data: 'btn_edit'},
                    {data: 'btn_hapus'},
                    // {
                    //     data: 'action',
                    //     name: 'action',
                    //     orderable: true,
                    //     searchable: true
                    // },
                ]
            });
        });

        function openModal() {
            $("#exampleModalLabel").html("Form Tambah Kegiatan")
            $("#btnSimpan").html('Simpan Kegiatan');
            $("#exampleModal").modal('show');
        }

        function closeModal(){
            $("#id").val("");
            $("#warna").val("");
            $("#tanggal").val("");
            $("#deskripsi").val("");
            $("#exampleModal").modal('hide');
        }

        function editHub(id){
            $.ajax({
                url: "/event/show/"+id,
                type: "GET",
                dataType: 'json',
                success: function (data){
                    $("#id").val(data.data.id);
                    $("#warna").val(data.data.warna_id);
                    $("#tanggal").val(data.data.tanggal);
                    $("#deskripsi").val(data.data.event);
                    $("#btnSimpan").html('Ubah Kegiatan');
                    $("#exampleModalLabel").html('Form Ubah Kegiatan');
                    $("#exampleModal").modal('show');
                }
            });
        }

        function hapusHub(id){
            if(confirm('Apakah anda yakin hapus data ini ?') === true){
                $.ajax({
                    url: "{{route('destroy.event')}}",
                    type: "POST",
                    dataType: 'json',
                    data: {
                        _token: '{{csrf_token()}}',
                        id: id
                    },
                    success: function (data){
                        if(data.code==='0'){
                            alert(data.msg);
                        }else{
                            location.reload();
                        }
                    }
                });
            }
        }

        function simpanHub(){
            $.ajax({
                url: "{{route('store.event')}}",
                type: "POST",
                dataType: 'json',
                data: $("#eventForm").serialize(),
                success: function (data){
                    if(data.code==='0'){
                        closeModal();
                        alert(data.msg);
                    }else{
                        location.reload();
                    }
                }
            });
        }

    </script>
@endpush
