<!DOCTYPE html>
<html>
<head>
    <title>Hello</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/demo.css')}}"/>
    <link rel="stylesheet" href="{{asset('css/theme2.css')}}"/>
</head>
<body style="background-image: url('{{asset('backdrop_new.png')}}');background-repeat: no-repeat;
  background-attachment: fixed;
  background-size: cover; opacity: 100; font-weight: bold">

<div class="container">
    <div class="card h-100 d-flex align-item-center mt-5 ">
        <div class="card-body p-4">
            <h1 class="text-center pb-4">Kalender akademik<br></h1>
            <div class="row">
                <div class="col-md-4 text-center"><img src="{{asset('logo_yayasan.png')}}" width="40%"></div>
                <div class="col-md-4"><img src="{{asset('banner.png')}}" width="100%" style="padding-top: 40px"></div>
                <div class="col-md-4 text-center"><img src="{{asset('logo_uht.png')}}" width="40%"></div>
            </div><br/>
            <div class="test p-4">
                <div id="caleandar"></div>
                <div style="padding-left: 50px; padding-right: 50px">
                    <table class="table">
                        @foreach($warnas as $warna)
                            <tr>
                                <td style="background-color: {{$warna->warna_background}}"></td>
                                <td style="color: white; font-weight: bold">{{$warna->warna_nama}}</td>
                            </tr>
                        @endforeach
                    </table>
{{--                    <div class="row">--}}
{{--                        @foreach($warnas as $warna)--}}
{{--                            <div class="col-md-3" style="background-color: {{$warna->warna_background}}"></div>--}}
{{--                            <div class="col-md-3" style="color: white; font-weight: bold">{{$warna->warna_nama}}</div>--}}
{{--                        @endforeach--}}
{{--                    </div>--}}
                </div>
            </div>
        </div>
    </div><br/><br/>
    <img src="{{asset('footer.png')}}" width="100%">
    <br/><br/>
</div>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
        crossorigin="anonymous"></script>
<script type="text/javascript" src="js/caleandar.js"></script>
{{--<script type="text/javascript" src="js/demo.js"></script>--}}
<script type="text/javascript">
    var events = [
        @foreach($events as $event)
        {'Date': new Date({{(int)explode('-',$event->tanggal)[0]}}, {{(int)explode('-',$event->tanggal)[1] - 1}}, {{(int)explode('-',$event->tanggal)[2]}}), 'Title': '{{$event->event}}. %{{$event->warna->warna_background}}%{{$event->warna->warna_font}}%', 'Link': '#'},
        @endforeach
        // {'Date': new Date(2022, 2, 16), 'Title': 'Doctor appointment at 3:25pm. %#ff8080%white%', 'Link': '#'},
        // {'Date': new Date(2022, 2, 17), 'Title': 'Doctor appointment at 3:25pm. %red%white%', 'Link': '#'},
        // {'Date': new Date(2022, 2, 18), 'Title': 'New Garfield movie comes out! %#8533ff%white%', 'Link': '#'},
        // {'Date': new Date(2022, 2, 27), 'Title': '25 year anniversary', 'Link': '#'},
    ];
    var settings = {};
    var element = document.getElementById('caleandar');
    caleandar(element, events, settings);

</script>
</body>
</html>
