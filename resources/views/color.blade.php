@extends('layout')

@section('warna', 'active')

@section('content')

    <div class="jumbotron">
        <h1>Group Kegiatan Akademik</h1>
        <h3>Berdasarkan Warna</h3>
        <button class="btn btn-lg btn-primary" onclick="openModal()">Tambah Group Warna</button>
    </div>
    <table class="table table-bordered" id="content">
        <thead>
        <tr>
            <th>No</th>
            <th>Warna Background</th>
            <th>Warna Font</th>
            <th>Nama</th>
            <th>Deskripsi</th>
            <th></th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" onclick="closeModal()">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="eventForm">
                        @csrf
                        <input type="hidden" name="id" id="id">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Warna Background</label>
                                <input type="color" name="background" id="background" class="form-control">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Warna Font</label>
                                <input type="color" name="font" id="font" class="form-control">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label>Nama Warna</label>
                                <input type="text" name="nama" id="nama" class="form-control">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label>Deskripsi</label>
                                <textarea class="form-control" name="deskripsi" id="deskripsi" rows="5"></textarea>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" onclick="closeModal()">Close</button>
                    <button type="button" class="btn btn-primary" onclick="simpanHub()" id="btnSimpan"></button>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('javascript')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#content').DataTable({
                processing: true,
                serverSide: true,
                ajax: "{{ route('warna') }}",
                columns: [
                    {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                    {data: 'warna_background'},
                    {data: 'warna_font'},
                    {data: 'warna_nama'},
                    {data: 'deskripsi'},
                    {data: 'btn_edit'},
                    {data: 'btn_hapus'},
                ]
            });
        });

        function openModal() {
            $("#exampleModalLabel").html("Form Tambah Warna")
            $("#btnSimpan").html('Simpan Warna');
            $("#exampleModal").modal('show');
        }

        function closeModal(){
            $("#id").val("");
            $("#background").val("");
            $("#font").val("");
            $("#nama").val("");
            $("#deskripsi").val("");
            $("#exampleModal").modal('hide');
        }

        function editHub(id){
            $.ajax({
                url: "/color/show/"+id,
                type: "GET",
                dataType: 'json',
                success: function (data){
                    $("#id").val(data.data.id);
                    $("#background").val(data.data.warna_background);
                    $("#font").val(data.data.warna_font);
                    $("#nama").val(data.data.warna_nama);
                    $("#deskripsi").val(data.data.deskripsi);
                    $("#btnSimpan").html('Ubah Warna');
                    $("#exampleModalLabel").html('Form Ubah Warna');
                    $("#exampleModal").modal('show');
                }
            });
        }

        function hapusHub(id){
            if(confirm('Apakah anda yakin hapus data ini ?') === true){
                $.ajax({
                    url: "{{route('destroy.warna')}}",
                    type: "POST",
                    dataType: 'json',
                    data: {
                        _token: '{{csrf_token()}}',
                        id: id
                    },
                    success: function (data){
                        if(data.code==='0'){
                            alert(data.msg);
                        }else{
                            location.reload();
                        }
                    }
                });
            }
        }

        function simpanHub(){
            $.ajax({
                url: "{{route('store.warna')}}",
                type: "POST",
                dataType: 'json',
                data: $("#eventForm").serialize(),
                success: function (data){
                    if(data.code==='0'){
                        closeModal();
                        alert(data.msg);
                    }else{
                        location.reload();
                    }
                }
            });
        }

    </script>
@endpush
